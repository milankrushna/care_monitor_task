/**
 * PGSQL Database Connection
 */
const { pgSQLEnvironment } = require("./../config/env");
const { Client } = require("pg");


const PG_CONFIG = {
    host: pgSQLEnvironment.pgsql_host,
    port: pgSQLEnvironment.pgsql_port,
    database: pgSQLEnvironment.pgsql_database,
    user: pgSQLEnvironment.pgsql_user,
    password: pgSQLEnvironment.pgsql_password,
    allowExitOnIdle: pgSQLEnvironment.pgsql_allowExitOnIdle,
    max: 50,
    keepAlive: true,
    idleTimeoutMillis: 30000,
    connectionTimeoutMillis: 2000,
};


const client = new Client(PG_CONFIG);
console.log("PGSQL Initiated");
client.connect((err) => {
    if (err) {
        console.error("PG connection error", err.stack);
    } else {
        console.log("PG connected");
    }
});

module.exports = { client };
