const moment = require("moment");

const { em, HEARTRATE_INSERT_LISTENER } = require("./../config/listener.config");
require("./../helper/heartrate.listener");
const filterHelper = require("./../helper/filter.helper");

/**
 * Process Heartrate Data
 * @param {*} req 
 * @param {*} res 
 */
exports.processHeartRate = (req, res) => {

    try {

        let { clinical_data, patient_id, from_healthkit_sync, orgId, timestamp } = req.body;

        let heartRatedata = clinical_data?.HEART_RATE?.data;// if data is sorted no need to run the next line
        // let heartRatedata = filterHelper.sortArrayList(clinical_data?.HEART_RATE?.data);//if data is not sorted uncomment this line and uncomment previoud one line

        let slotGap = 15; //min

        let aggregateData = [];

        let from_date = moment(heartRatedata[0]["on_date"]),
            to_date = moment(heartRatedata[0]["on_date"]).add(slotGap, "minute"),
            low = +heartRatedata[0]["measurement"],
            high = +heartRatedata[0]["measurement"];
        // console.log(from_date.valueOf() - to_date.valueOf());
        for (let i = 0; i < heartRatedata.length; i++) {

            if (moment(heartRatedata[i]["on_date"]).valueOf() <= to_date.valueOf()) {

                if (+heartRatedata[i]["measurement"] < low) {
                    // console.log("low", heartRatedata[i]["measurement"]);
                    low = +heartRatedata[i]["measurement"];
                } else if (+heartRatedata[i]["measurement"] > high) {
                    // console.log("high", heartRatedata[i]["measurement"]);
                    high = +heartRatedata[i]["measurement"];
                }


            } else {

                let processData = {
                    from_date: from_date.toISOString(),
                    to_date: to_date.toISOString(),
                    "measurement": {
                        low,
                        high
                    }
                };
                aggregateData.push(processData);

                setImmediate(() => {
                    // console.log("333");
                    em.emit(HEARTRATE_INSERT_LISTENER, processData);
                });

                from_date = moment(heartRatedata[i]["on_date"]);
                to_date = moment(heartRatedata[i]["on_date"]).add(slotGap, "minute");
                low = high = +heartRatedata[i]["measurement"];

            }
        }
        console.log(aggregateData.length);
        res.status(200).json({ status: 0, message: "Data Process Successfully", data: { patient_id, from_healthkit_sync, orgId, timestamp, aggregateData } });


    } catch (error) {
        res.status(422).json({ status: -1, message: error.message });
    }
};