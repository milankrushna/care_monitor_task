/**
 * PQSQL Query exec
 */
const { client } = require("./../cred/pgsql");
const { HEART_MONITOR } = require("../config/pgTable.config");
// var events = require("events");
// var eventEmitter = new events.EventEmitter();

/**
 * heart rate date insert
 * @param {from_date,to_date,high,low} insertObj 
 */
exports.insertHeartRate = (insertObj) => {

    return client.query(`INSERT INTO ${HEART_MONITOR} (from_date, to_date, high, low) VALUES ($1, $2, $3, $4)`, insertObj)
        .then((res) => console.log(res.command, "Data Successfully Inserted", insertObj))
        .catch((err) => console.log("error", err.message, insertObj));

};