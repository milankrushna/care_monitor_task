/* eslint-disable no-undef */
module.exports = {
    env: {
        browser: true,
        es2021: true,
        jasmine: true,
    },

    parser: "@typescript-eslint/parser",
    parserOptions: {
        ecmaVersion: "latest",
        sourceType: "module",
    },
    plugins: ["@typescript-eslint", "prettier"],
    rules: {
        "semi": ["error", "always"],
        "no-unused-vars": ["warn", { "vars": "all" }],
        "quotes": [2, "double", { "avoidEscape": true, "allowTemplateLiterals": true }],
        "no-redeclare": ["error", { "builtinGlobals": true }],
        "no-use-before-define": ["error", { "functions": true, "classes": true, "variables": true }],
        "consistent-return": 2,
        // "indent"           : [2, 4],
        "no-else-return": 1,
        "semi": [1, "always"],
        "space-unary-ops": 2
    },
    overrides: [
        {
            files: ["*.spec.ts"],
            plugins: ["jasmine"],
            rules: {
                "@typescript-eslint/unbound-method": "off",
                MicrosoftEdge: "off",
                "axe/forms": "off",
            },
        },
    ],
};