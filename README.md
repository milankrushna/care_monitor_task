
# Heart Rate Monitor

NodeJS based API to receive, process, and store Heart Rate data asynchronously.


## Tech Stack

**Server:** Node, Express

**DataBase:** PostgreSQL


## Installation

Clone the project

```bash
  git clone https://gitlab.com/milankrushna/care_monitor_task.git
```

Go to the project directory

```bash
  cd care_monitor_task
```

Install dependencies

```bash
  npm install
```

## Run Localy

Start the server

```bash
  npm run start
```


## Environment Variables

To run this project, you will need to add the following environment variables to the env.development/env.production file accordingly.
```
PORT
pgSqlDBenvironment: {
        pgsql_host: "",
        pgsql_port: "",
        pgsql_database: "",
        pgsql_user: "",
        pgsql_password: "",
        pgsql_allowExitOnIdle: ",
}

```

## Table Details
**TableName :** heart_rate_monitor
|#|Column_name|Data Type|is_nullable|
|:----|:----|:----|:----|
|1|id|int4|NO|
|2|from_date|timestamptz|YES|
|3|to_date|timestamptz|YES|
|4|low|int8|YES|
|5|high|int8|YES|

## Index Details
|index_name|index_algorithm|is_unique|
|:----|:----|:----|
|heart_rate_monitor_pkey|BTREE|TRUE|

## API Reference

#### **Process Heart rate data**

```http
  POST /api/process_heart_rate
```
#### Request Body

```
{
   "clinical_data":{
      "HEART_RATE":{
         "uom":"beats/min",
         "data":[
            {
               "on_date":"2020-10-06T06:48:17.503000Z",
               "measurement":"111"
            },
            {
               "on_date":"2020-10-06T06:48:38.065000Z",
               "measurement":"66"
            }],
         "name":"Steps"
      }
   },
   "patient_id":"gk6dhgh-9a60-4980-bb8b-787bf82689d7",
   "from_healthkit_sync":true,
   "orgId":"8gj4djk6s-a5ad-444b-b58c-358dcbd72dc3",
   "timestamp":"2020-10-09T05:36:31.381Z"
}
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `uom` | `string` | Heart rate unit |
| `on_date` | `ISO timestamp` | heart rate taken timestamp |
| `patient_id` | `string` | Patient Unique Id |
| `from_healthkit_sync` | `Boolean` |  |
| `orgId` | `string` | Medical Id |
| `timestamp` | `ISO Timestamp` | Data received time |

#### Response

```
HTTP/1.1 200 OK
Status: 200 OK
Content-Type: application/json

Response Body
{
    "status": 0,
    "message": "Data Process Successfully",
    "patient_id": "gk6dhgh-9a60-4980-bb8b-787bf82689d7",
    "from_healthkit_sync": true,
    "orgId": "8gj4djk6s-a5ad-444b-b58c-358dcbd72dc3",
    "timestamp": "2020-10-09T05:36:31.381Z",
    "data": [
        {
            "from_date": "2020-10-06T06:48:17.503Z",
            "to_date": "2020-10-06T07:03:17.503Z",
            "measurement": {
                "low": 66,
                "high": 148
            }
        },
        {
            "from_date": "2020-10-06T07:15:06.645Z",
            "to_date": "2020-10-06T07:30:06.645Z",
            "measurement": {
                "low": 134,
                "high": 134
            }
        }
    ]
}

```

## Features

- Aggregate min and max heart rate for every 15 (configurable) minutes.
- Return the processed response including HEART_RATE and other metrics.
- Data inserting into Database
- Cross platform


## Authors

- [@milankrushna](https://github.com/milankrushna)


## License

[MIT](https://choosealicense.com/licenses/mit/)

