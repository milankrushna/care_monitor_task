/**
 * async event Listener
 */

const events = require("events");
const em = new events.EventEmitter();


module.exports = {
    em,
    HEARTRATE_INSERT_LISTENER: "heart_rate_data_in"

};