const app = require("./server/app.server");
const ENVIRONMENT = require("./config/env");

const PORT = ENVIRONMENT.PORT || 8080;

/**
 * starting the server
 */
let server = app.listen(PORT, () => {
  console.log(`Server Listening on ${PORT}`);
});

//server will get timeout after 1min
server.setTimeout(60000);
