var express = require("express");
var router = express.Router();
const heartRateController = require("./../controllers//heartrate.controller");

/**
 * heartRate Route
 */
router.post("/process_heart_rate", heartRateController.processHeartRate);

module.exports = router;
