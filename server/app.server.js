const express = require("express");
const cors = require("cors");
const app = express();

/**
 * body parser configuration
 */
app.use(express.json({ limit: "10mb" }));
app.use(express.urlencoded({ extended: false }));
app.use(cors());


app.use((err, req, res, next) => {
  console.log("--->", "requetBody Verify");
  if (err instanceof SyntaxError && err.status === 400 && "body" in err) {
    console.error("invalid", err);
    return res
      .status(404)
      .json({ status: -1, message: "put a valid request body" }); // Bad request
  }
  // return null;

  return next();
});

let indexRouter = require("./../routes/index");
// let usersRouter = require("./../routes/users");

app.use("/api/", indexRouter);
// app.use("/users", usersRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  res.status(404).json({ status: -1, "message": "404 No route found" });
  next();
});


// error handler
app.use(function (err, req, res,) {

  res.status(err.status || 500);
  res.json({ status: -1, message: err.message });
});

module.exports = app;