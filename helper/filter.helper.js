const moment = require("moment");

/**
 * sort the array if data is not sorted according to date time
 * @param {*} arrayData 
 * @returns 
 */
exports.sortArrayList = (arrayData) => {

    if (Array.isArray(arrayData)) {
        return arrayData.sort((a, b) => {
            return (moment(a.on_date).valueOf() - moment(b.on_date).valueOf());
        });
    }
    return [];

};

