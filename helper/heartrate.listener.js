/**
 * ASYNC DB Opertation through event Listener
 */
const { em, HEARTRATE_INSERT_LISTENER } = require("./../config/listener.config");
const heartRateModel = require("./../models/hm.model");

/**
 * heartRate data insert listener
 */
em.on(HEARTRATE_INSERT_LISTENER, async (...arg) => {

    let { from_date, to_date, measurement: { high, low } } = arg[0];
    // console.log([from_date, to_date, high, low]);
    heartRateModel.insertHeartRate([from_date, to_date, high, low]);

});
